# Programming School Work
The following repository is being created for me to store my college/university programming assignments. Both homework and in-class assignments will be uploaded. Doing this with the purpose of tracking my progress and for reference if I ever need to look back on solutions.

I'll do my best to keep this sorted and as clean as possible. :)
