# C++ Pointers homework assignment
For COSC 1437 (Programming Fundamentals II), our professor gave us the following homework assignment.

> Translate the psudo code to a working c++ program.
> Implement indirect and direct pointers to replace array call.

This was the code that I submitted to complete the assignment. Our professor makes us add the "System('Pause')" before the return function so he can verify the output in Windows without the terminal window closing. I removed that so the program had no dependencies. You can compile it on Linux by using the following command.

```sh
[~]# gcc pointers.cpp -lstdc++ -o pointers.o
[~]# stat pointers.o
  File: ‘pointers.o’
```

In the command above, stat was used to confirm the file creation.
