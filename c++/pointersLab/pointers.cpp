#include <iostream>
#include <string>
using namespace std;

int main()
{
	string name;
	string address;
	int item, quantity;
	double price;
	int const SIZE = 6;
	
	int VALID_ITEM[SIZE] = { 106, 108, 307, 405, 457, 688};	
	int *vI = VALID_ITEM;
	double VALID_ITEM_PRICE[SIZE] = { 0.59, 0.99, 4.50, 15.99, 17.50, 39.00};
	double *vIP = VALID_ITEM_PRICE;
        int sub;

	string foundIt = "N";
	string MSG_YES = "Item available";
	string MSG_NO = "Item not found";

	cout << "Please enter name: ";
	cin >> name;
	cout << "Please enter your address: ";
	cin.ignore(); // flush input stream.
	getline(cin, address);
	cout << "Please enter the item number: ";
	cin >> item;
	cout << "Please enter the quantity of the item: ";
	cin >> quantity;
	
	sub = 1;
	while(sub <= SIZE) {
		//if (item == VALID_ITEM[sub]) { // Using array directly
		//if (item == *(VALID_ITEM + sub)) { // Using indirect pointer on array to get item.
		if (item == *vI) { // Using pointer 
			foundIt = "Y";
			//price = VALID_ITEM_PRICE[sub]; // Using array directly
			//price = *(VALID_ITEM_PRICE + sub); // Using indirect pointer on array to get item.
			price = *vIP; // Using pointer			
		}
		*vI++; // Move on to the next VALID_ITEM array memory address.
		*vIP++; // Move on to the next VALID_ITEM_PRICE array memory address.
		sub++;
	}

	if (foundIt == "Y") {
		cout << MSG_YES << endl;
		cout << quantity << " at " << (quantity * price) << endl;
	} else {
		cout << MSG_NO << endl;
	}	
	return 0;
}
