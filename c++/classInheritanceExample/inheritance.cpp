#include <iostream>
#include <string>
using namespace std;

// Person is the base class that I will use to inherit from.
class person {
private:
	// Declare first name and last name variables.
	string firstName;
	string lastName;
public:
	// Create object constructors/deconstructors.
	person();
	person(string fname, string lname);
	~person();
	// Functions to return first/last name.
	string displayFirstN();
	string displayLastN();
};

// Default constructor for Person class.
person::person() 
{
	firstName = "First";
	lastName = "Last";
}

// Constructor for Person class with arguments.
person::person(string fname, string lname) 
{
	firstName = fname;
	lastName = lname;
}

// Person object deconstructor.
person::~person()
{
	// Deconstructor
}

// Return the first name of Person object.
string person::displayFirstN() 
{
	return firstName;
}

// Return the last name of Person object.
string person::displayLastN() 
{
	return lastName;
}

// Create Student class and inherit public from Person base class.
class student : public person 
{
private:
	// Declare variables to store student ID, graduation year and degree plan.
	int studentID, gradYear;
	string degreePlan;
public:
	// Student constructors/deconstructors.
	student();
	student(string fN, string lN, int sID, int gradY, string degreeP);
	~student();
	// Void function to display data.
	void dispstudent();
};

// Default student object constructor.
student::student():person()
{
	studentID = 0;
	gradYear = 1970; // because Unix :).	
	degreePlan = "None";
}

// student constructor to create student object, uses person base class.
student::student(string fN, string lN, int sID, int gradY, string degreeP):person(fN, lN)
{
	// Assign arguments to existing student class variables.
	studentID = sID;
	gradYear = gradY;
	degreePlan = degreeP;
}

// Destroy student object on exit. 
student::~student() 
{
	// student deconstructor
}

// Our student class void function to display student information.
void student::dispstudent() 
{
	cout << "Name: " << displayFirstN() << ", " << displayLastN() <<endl;
	cout << "student ID: " << studentID << endl;
	cout << "Graduation Year: " << gradYear << endl;
	cout << "Degree Plan: " + degreePlan << endl;
}

// Create employee class that inherits from person base class.
class employee : public person
{
private:
	string title;
	int	amountOfYearsWorked;
public:
	employee();
	employee(string fN, string lN, string title, int years);
	~employee();
	void dispEmp();
};


employee::employee():person() 
{
	title = "N/A";
	amountOfYearsWorked = 0;
}

employee::employee(string fN, string lN, string t, int y):person(fN, lN) 
{
	title = t;
	amountOfYearsWorked = y;
}

employee::~employee()
{
	// employee Deconstructor
}

void employee::dispEmp()
{
	cout << "Name: " << displayFirstN() << ", " << displayLastN() << endl;
	cout << "Company position: " + title << endl;
	cout << "Years with company: " << amountOfYearsWorked << endl;
}

int main()
{
	// Create object studentOne and call constructor with arguments.
	student studentOne("Darwin", "Waterson", 500, 2020, "Computer Science");
	
	// Display student information.
	studentOne.dispstudent();

	// 2 new lines to space data.
	cout << "\n\n";

	// Create object employeeOne and call constructor with arguments.
	employee employeeOne("Gumball", "Waterson", "Comedian", 5);

	// Display employee information.
	employeeOne.dispEmp();

	return 0;
}

