# C++ Base Class Inheritance Example
Just an example on c++ base class inheritance. Reviewing for an upcoming test on 03/22/2012.

If you are on Linux and would like to compile and run this example please run the command below.

```sh
[~]# gcc inheritance.cpp -lstdc++ -o inheritanceprogram.o
[~]# chmod +x inheritanceprogram.o
[~]# ./inheritanceprogram.o
```
